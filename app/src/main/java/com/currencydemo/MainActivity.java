package com.currencydemo;

import android.content.Context;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

public class MainActivity extends AppCompatActivity {

    private Context context;

    private double[] currencyList = {1000, 500, 200, 100, 50, 20, 10, 5, 2, 0.5};

    private HashMap<Double, Integer> currencyMap = new LinkedHashMap<>();

    private EditText etEnterCurrency, etTotalPaid;
    private TextView tvCurrency;
    private AppCompatButton btnSubmit;

    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initlization();
        idMappings();
        setEventListener();
    }

    private void initlization() {
        context = this;
    }

    private void setEventListener() {

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                // hide KeyBoard when User Press Submit Button
                hideKeyBoard();
                String currencyText;
                if (currencyMap != null && !currencyMap.isEmpty()) {
                    currencyMap.clear();
                }


                /**
                 *  validate Data
                 */
                if (etEnterCurrency.getText().toString().equalsIgnoreCase("")) {
                    Toast.makeText(context, "Please Enter Total Bill", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (etTotalPaid.getText().toString().equalsIgnoreCase("")) {
                    Toast.makeText(context, "User Give Money", Toast.LENGTH_SHORT).show();
                    return;
                }


                /**
                 *   Get Value from input field
                 */
                String billPayment = etEnterCurrency.getText().toString();
                String totalPaidMoney = etTotalPaid.getText().toString();


                /**
                 *  convert All Value to Double
                 */
                double billPaymentDouble = Double.parseDouble(billPayment);
                double paidMoney = Double.parseDouble(totalPaidMoney);


                /**
                 *  validate Data
                 */
                if (paidMoney < billPaymentDouble) {
                    Toast.makeText(context, "Not enough money", Toast.LENGTH_SHORT).show();
                    return;
                }


                double difference = paidMoney - billPaymentDouble;

                // if same money than no need to calculation
                if (difference == 0) {
                    Toast.makeText(context, "No need to give anything return", Toast.LENGTH_SHORT).show();
                }

                // calculate Currency
                if (difference > 0) {
                    currencyText = "Currency:" + "\n";

                    /**
                     *  clear Textview before Load New Data
                     */
                    tvCurrency.setText("");

                    new CurrencyExchangeAsyncTask(difference,currencyText).execute();

                }
            }
        });

    }


    /**
     *
     */
    private void hideKeyBoard() {
        View view = getCurrentFocus();
        if (view != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            if (inputMethodManager != null) {
                inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
        }
    }

    private void idMappings() {

        progressBar=(ProgressBar) findViewById(R.id.progressBar);

        etEnterCurrency = (EditText) findViewById(R.id.etEnterCurrency);
        etTotalPaid = (EditText) findViewById(R.id.etTotalPaid);
        tvCurrency = (TextView) findViewById(R.id.tvCurrency);
        btnSubmit = (AppCompatButton) findViewById(R.id.btnSubmit);
    }


    /**
     * calculate Total currency require for exchange
     *
     * @param inputCurrency
     * @return
     */

    public  void calculateTotalCurrencyRequireForExchange(double inputCurrency) {
        // Error Check, If the input is 0 or lower, return 0.
        if (inputCurrency <= 0) return;

        // Number of Total Currency Exchange.
        for (int i = 0; i < currencyList.length; i++) {
            // Get the Currency from Array.
            double currency = currencyList[i];

            // If there is no inputCurrency  Left, simply break the for-loop
            if (inputCurrency == 0) break;

            // Check If we have a smaller input than our exchange
            // If it's, we need to go the Next one in our exchange Array.

            if (inputCurrency < currency) continue;

            // Get Remaining money
            double quotient = inputCurrency - currency;


            // if same money we start again from same
            if (quotient >= currency) {
                i--;
            }


            // Update the input with remaining money.
            inputCurrency = quotient;

            /**
             *  check Total count of currency exchange
             */
            if (currencyMap.containsKey(currency)) {
                currencyMap.put(currency, currencyMap.get(currency) + 1);
            } else {
                currencyMap.put(currency, 1);
            }

        }


    }


    /**
     *  handle Calcualtion in Background
     */
    private  class  CurrencyExchangeAsyncTask extends AsyncTask<Void,Void,Void>{
        private double difference;
        private String currencyText;

        public CurrencyExchangeAsyncTask(double difference, String currencyText) {
            this.difference = difference;
            this.currencyText = currencyText;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected Void doInBackground(Void... voids) {
            calculateTotalCurrencyRequireForExchange(difference);
            Set<Map.Entry<Double, Integer>> entries = currencyMap.entrySet();
            for (Map.Entry<Double, Integer> map : entries) {
                DecimalFormat decimalFormat = new DecimalFormat("#.##");
                currencyText = String.format("%s%s Count %s\n", currencyText, decimalFormat.format(map.getKey()), decimalFormat.format(map.getValue()));
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            progressBar.setVisibility(View.GONE);
            tvCurrency.setText(currencyText);

        }
    }


}
